import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent implements OnInit {

  directory = [
    {
      title: 'item 1',
      // tslint:disable-next-line: max-line-length
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originall y bred for hunting.'
    }, {
      title: 'item 2',
      // tslint:disable-next-line: max-line-length
      description: 'The Shiba Inu is the smallest bred for hunting.'
    }, {
      title: 'item 3',
      // tslint:disable-next-line: max-line-length
      description: 'The Shiba Inu is.'
    }, {
      title: 'item 4',
      // tslint:disable-next-line: max-line-length
      description: 'ix original and distinct spitz breeds of dog from Japan. A small, agile do'
    }, {
      title: 'item 5',
      // tslint:disable-next-line: max-line-length
      description: 'mountainous terrain, the Shiba Inu was originall'
    }, {
      title: 'item 6',
      // tslint:disable-next-line: max-line-length
      description: 'The Shiba hunting.'
    }, {
      title: 'item 7',
      // tslint:disable-next-line: max-line-length
      description: 'Te smallest bred for hunting.'
    }, {
      title: 'item 8',
      // tslint:disable-next-line: max-line-length
      description: 'The the smallest br'
    }, {
      title: 'item 9',
      // tslint:disable-next-line: max-line-length
      description: 'hunting asda li  ashd oiasda.'
    }
  ];

  tabs = [
    {
      name: 'Tab 1',
      directory: this.directory
    }, {
      name: 'Tab 2',
      directory: this.directory
    }, {
      name: 'Tab 3',
      directory: this.directory
    }, {
      name: 'Tab 4',
      directory: this.directory
    }, {
      name: 'Tab 5',
      directory: this.directory
    }, {
      name: 'Tab 6',
      directory: this.directory
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
