import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatComponent } from './views/chat/chat.component';
import { DirectoryComponent } from './views/directory/directory.component';
import { ProcessIpComponent } from './views/process-ip/process-ip.component';

const routes: Routes = [
  { path: '',   redirectTo: '/caht', pathMatch: 'full' },
  { path: 'chat', component: ChatComponent },
  { path: 'directorio', component: DirectoryComponent },
  { path: 'proceso-ip', component: ProcessIpComponent },
  { path: '**', component: ChatComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
