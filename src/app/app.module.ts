import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// componentes del angular material
import { MaterialModule } from './material-modules';
// componentes 
import { MainComponent } from './views/main/main.component';
import { ChatComponent } from './views/chat/chat.component';
import { DirectoryComponent } from './views/directory/directory.component';
import { ProcessIpComponent } from './views/process-ip/process-ip.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ChatComponent,
    DirectoryComponent,
    ProcessIpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
