import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  chat = [
    {
      from: 'robot',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.',
      options: [
        {
          text: 'Opcion 1'
        }, {
          text: 'Opcion 2'
        }, {
          text: 'Opcion 3'
        }, {
          text: 'Opcion 4'
        }
      ]
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'robot',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'robot',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'robot',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'robot',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    },
    {
      from: 'user',
      // tslint:disable-next-line: max-line-length
      text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti, repellendus molestias. Doloremque, nemo similique quaerat ut tempora neque provident, nesciunt blanditiis perspiciatis quod assumenda corporis voluptatum totam ipsum voluptate architecto.'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
