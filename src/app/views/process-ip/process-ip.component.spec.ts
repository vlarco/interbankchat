import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessIpComponent } from './process-ip.component';

describe('ProcessIpComponent', () => {
  let component: ProcessIpComponent;
  let fixture: ComponentFixture<ProcessIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
