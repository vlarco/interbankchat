import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnDestroy {

  sizeMenu = false;

  contraerFlag = false;

  mobileQuery: MediaQueryList;

  headerSideNav = 'Selecciona un producto para empezar';
  sideNavContent = [
    {
      description: 'Tarjeta de crédito',
      icon: 'credit_card'
    },
    {
      description: 'Tarjeta de débito',
      icon: 'card_membership'
    },
    {
      description: 'Préstamos',
      icon: 'attach_money'
    },
    {
      description: 'Seguros',
      icon: 'local_hospital'
    },
    {
      description: 'Campañas',
      icon: 'confirmation_number'
    }
  ];

  private mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      if (this.mobileQuery.matches) {
        this.contraerFlag = false;
      }
    };
    // tslint:disable-next-line: deprecation
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }

  hideShowMenu(val: any) {
    this.contraerFlag = val;
  }

}
